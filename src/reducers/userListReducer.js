import _ from 'lodash';

const defaultState = {
  users: [],
  loading: true,
  deleteLoading: 0,
  error: null,
  page: 1,
};

export default (state = defaultState, action) => {
  console.log('ACTION -> ', action.type, action)
  
  switch (action.type) {
    case 'FETCH_USER_LIST_BEGIN':
      return {
        ...state,
        loading: true,
        error: null,
      }

    case 'FETCH_USER_LIST_SUCCESS':

      return {
        ...state,
        loading: false,
        users: action.payload.users.data,
        page: action.payload.page + 1,
      };

    case 'FETCH_USER_LIST_FAILURE':
      return {
        ...state,
        users: [],
        loading: false,
        error: action.payload.error,
      };

    case 'DELETE_USER_BEGIN':
    
      return {
        ...state,
        error: null,
        deleteLoading: action.id,
      }

    case 'DELETE_USER_SUCCESS':
      return {
        ...state,
        users: state.users.filter(user => user.id !== action.id),
        deleteLoading: 0,
      };

    case 'DELETE_USER_FAILURE':
      return {
        ...state,
        error: action.payload.error,
        deleteLoading: 0,
      };

    default:
      return state
  }
}