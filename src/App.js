import React, { Component } from 'react';
import { connect } from 'react-redux';

import UserListContainer from './containers/UserListContainer';

class App extends Component {

  render() {

    return (
      <div>
        <UserListContainer />
      </div>
    );
  }
}

export default App;
