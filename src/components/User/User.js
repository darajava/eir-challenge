import * as React from "react";
import styles from './User.module.css';



export default (props) => {
  const user = props.user;

  let edit = () => {
    // if (props.editing) {
      props.setEditable();
    // } else {
      // props.updateUser(document.getElementById('first_name'), document.getElementById('last_name'))
    // }
  }

  return (
    <div className={styles.container} onClick={() => {}}>
      <div>
        <img className={styles.thumbnail} src={user.avatar} />
      </div>
      <div>
        <div className={styles.title}>
          {!props.editing ? 
            (
            <div>
              <span>
              {user.first_name}
            </span>
            <span>
              {" " + user.last_name}
            </span>
            </div>
            )
            :
            (
            <div>
              <input value={user.first_name} />
              <input value={user.last_name} />
            </div>
            )
          }
        </div>
      </div>
      <div className={styles.actions}>
        <div className={styles.button + " " + styles.edit} onClick={edit}>{"edit"}</div>
        <div className={styles.button + " " + styles.delete}  onClick={() => props.deleteUser(user.id)}>{props.loading ? "deleting..." : "delete"}</div>
      </div>
    </div>
  );
}