import * as React from "react";
import styles from './UserList.module.css';


export default (props) => {

  return (
    <div className={(props.wide ? styles.wide : "") + " " + styles.container}>
      {props.children}
    </div>
  );
}