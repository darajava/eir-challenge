export const fetchUserList = (page) => {

  return dispatch => {
    dispatch(fetchUserListBegin());
    return fetch(`https://reqres.in/api/users?page=${page}`)
      .then(handleErrors)
      .then(res => res.json())
      .then(json => {
        dispatch(fetchUserListSuccess(json));
        return json;
      })
      .catch(error => dispatch(fetchUserListFailure(error)));
  };
}

function handleErrors(response) {
  if (!response.ok) {
    // console.error(response)
    throw Error(response.statusText);
  }
  return response;
}

export const fetchUserListBegin = () => ({
  type: "FETCH_USER_LIST_BEGIN"
});

export const fetchUserListSuccess = users => ({
  type: "FETCH_USER_LIST_SUCCESS",
  payload: { users }
});

export const fetchUserListFailure = error => ({
  type: "FETCH_USER_LIST_FAILURE",
  payload: { error }
});


export const deleteUser = (id) => {

  return dispatch => {
    dispatch(deleteUserBegin(id));
    return fetch(`https://reqres.in/api/users/${id}`, {method: "delete"})
      .then(handleErrors)
      .then(json => {
        dispatch(deleteUserSuccess(id));
        return json;
      })
      .catch(error => dispatch(deleteUserFailure(error)));
  };
}

export const deleteUserBegin = (id) => ({
  type: "DELETE_USER_BEGIN",
  id: id,
});


export const deleteUserSuccess = (id) => ({
  type: "DELETE_USER_SUCCESS",
  id: id
});

export const deleteUserFailure = error => {
  console.log(error)
  return ({
    type: "DELETE_USER_FAILURE",
    payload: { error }
  })
};