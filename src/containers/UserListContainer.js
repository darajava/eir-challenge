import React, { Component } from 'react';
import { connect, dispatch } from 'react-redux';

import { fetchUserList, deleteUser } from '../actions/userListActions';
import UserList from '../components/UserList/UserList';
import User from '../components/User/User';

const mapStateToProps = state => ({
  users: state.userListReducer.users,
  error: state.userListReducer.error,
  loading: state.userListReducer.loading,
  deleteLoading: state.userListReducer.deleteLoading,
});

const mapDispatchToProps = dispatch => ({
 fetchUserList: (page) => dispatch(fetchUserList(page)),
 deleteUser: (id) => dispatch(deleteUser(id)),
 // updateUser: (userId, field, value) => dispatch(updateUser(userId, field, value)),
});

class UserListContainer extends Component {

  constructor (props) {
    super(props);
    this.state = {
      editing: false,
    }
  }

  componentDidMount() {
    this.props.fetchUserList(1);

  }

  setEditable = () => {
    this.setState({
      editing: !this.state.editing,
    })
  }

  render() {
    const { error, loading, users, deleteLoading } = this.props;

    if (error) {
      return <div>Error! {error.message}</div>;
    }

    if (loading) {
      return <div>Loading...</div>;
    }

    return (
      <UserList loading={this.props.loading}>
        {
          users && users.map((user) => {
            return <User key={user.id} editing={this.state.editing} loading={deleteLoading === user.id} user={user} setEditable={this.setEditable} deleteUser={this.props.deleteUser}/>;
          })
        }
      </UserList>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserListContainer);
