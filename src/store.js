import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import userListReducer from './reducers/userListReducer';
// import userReducer from './reducers/userReducer';

export default function configureStore(initialState) {
  return createStore(
    combineReducers({
      userListReducer,
      // userReducer,
    }),
    applyMiddleware(thunk)
  );
}